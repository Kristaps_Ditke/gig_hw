﻿using BookRentalAPI;
using BookRentalAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace BookRentalData
{
    public class BookRentalDbContext : DbContext
    {
        public BookRentalDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Book> AllBooks { get; set; }
        public DbSet<Customer> AllCustomers { get; set; }
    }
}
