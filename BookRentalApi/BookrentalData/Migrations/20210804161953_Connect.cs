﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookRentalData.Migrations
{
    public partial class Connect : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AllCustomers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(type: "TEXT", nullable: false),
                    LastName = table.Column<string>(type: "TEXT", nullable: false),
                    Email = table.Column<string>(type: "TEXT", nullable: true),
                    Address = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllCustomers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AllBooks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Author = table.Column<string>(type: "TEXT", nullable: false),
                    DateAcquired = table.Column<string>(type: "TEXT", nullable: true),
                    RentedToId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllBooks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AllBooks_AllCustomers_RentedToId",
                        column: x => x.RentedToId,
                        principalTable: "AllCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AllBooks_RentedToId",
                table: "AllBooks",
                column: "RentedToId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AllBooks");

            migrationBuilder.DropTable(
                name: "AllCustomers");
        }
    }
}
