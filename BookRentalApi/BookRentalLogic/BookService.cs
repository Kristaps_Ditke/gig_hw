﻿using BookRentalAPI.Models;
using BookRentalBase;
using BookRentalData;
using BookRentalLogic.Exceptions;
using BookRentalLogic.Validations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace BookRentalLogic
{
    public class BookService : DbService, IBookService
    {
        private readonly IBookIsRented _isRented;
        private readonly IAvailableToRent _customer;
        private readonly ICustomerService _service;
        public BookService(BookRentalDbContext context,
            ICustomerService service,
            IBookIsRented isRented,
            IAvailableToRent customer) : base(context)
        {
            _isRented = isRented;
            _customer = customer;
            _service = service;
        }

        public async Task<Book> GetBookByIdAsync(int id)
        {
            return await _context.Set<Book>().Include(book => book.RentedTo ).SingleOrDefaultAsync(book => book.Id == id);
        }

        public async Task<Book> RentBookAsync(int customerId, int bookId)
        {
            
            var bookToRent = await GetByIdAsync<Book>(bookId);
            var customer = await _service.GetCustomerByIdAsync(customerId);
            if (bookToRent == null || customer == null)
            {
                throw new ItemNullException("There is no such items in database. Provide correct Id's!");
            }

            if (_isRented.Validate(bookToRent))
            {
                throw new BookRentedException("The chosen book is already rented!");
            }

            if ( ! _customer.Validate(customer))
            {
                throw new RentLimitException("The customer is not allowed to rent a third book!");
            }

            bookToRent.RentedTo = customer;
            await UpdateAsync(bookToRent);
            return bookToRent;
        }

        public async Task<Book> ReturnBookAsync(int bookId)
        {
            var bookToReturn = await GetBookByIdAsync(bookId);

            if (bookToReturn == null)
            {
                throw new ItemNullException();
            }

            bookToReturn.RentedTo = null;
            await UpdateAsync(bookToReturn);
            return bookToReturn;
        }

        public async Task<Book> CreateAsync(Book bookToCreate)
        {
            bookToCreate.DateAcquired = DateTime.Now.ToString("dd/MM/yyyy");
           return await CreateAsync<Book>(bookToCreate);
        }
    }
}
