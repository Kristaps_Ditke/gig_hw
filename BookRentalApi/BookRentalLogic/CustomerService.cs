﻿using BookRentalAPI;
using BookRentalBase;
using BookRentalData;
using BookRentalLogic.Exceptions;
using BookRentalLogic.Validations;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace BookRentalLogic
{
    public class CustomerService : DbService, ICustomerService
    {
        protected readonly IReadyToBeRemoved _toRemove;
        public CustomerService(BookRentalDbContext context, IReadyToBeRemoved toRemove) : base(context)
        {
            _toRemove = toRemove;
        }

        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            return await _context.Set<Customer>().Include("CurrentlyRentedBooks").SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Customer> DeleteCustomerByIdAsync(int id)
        {
            var toBeRemoved = await GetCustomerByIdAsync(id);

            if (toBeRemoved == null)
            {
                throw new ItemNullException();
            }

            if ( ! _toRemove.Validate(toBeRemoved))
            {
                throw new RentLimitException("Customer has not returned all the rented books yet!");
            }

            await DeleteAsync<Customer>(id);
            return toBeRemoved;
        }
    }
}
