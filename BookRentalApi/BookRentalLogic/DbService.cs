﻿using BookRentalBase;
using BookRentalBase.Models;
using BookRentalData;
using BookRentalLogic.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace BookRentalLogic
{
    public class DbService : IDbService
    {
        protected readonly DbContext _context;
    
        public DbService(BookRentalDbContext context)
        {
            _context = context;
        }

        public IQueryable<T> Query<T>() where T : Entity
        {
            return _context.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAsync<T>() where T : Entity
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync<T>(int id) where T : Entity
        {
            return await _context.Set<T>().SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<T> CreateAsync<T>(T entity) where T : Entity
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<T> UpdateAsync<T>(T entity) where T : Entity
        {
            var forUpdate = await GetByIdAsync<T>(entity.Id);

            if (forUpdate == null)
            {
                throw new ItemNullException();
            }

            _context.Entry<T>(forUpdate).CurrentValues.SetValues(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<T> DeleteAsync<T>(int id) where T : Entity
        {
            var toBeRemoved = await GetByIdAsync<T>(id);

            if (toBeRemoved == null)
            {
                throw new ItemNullException("No such item in database!");
            }

            _context.Set<T>().Remove(toBeRemoved);
            await _context.SaveChangesAsync();
            return toBeRemoved;
        }
    }
}
