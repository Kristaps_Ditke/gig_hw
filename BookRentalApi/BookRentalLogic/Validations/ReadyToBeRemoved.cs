﻿using BookRentalAPI;

namespace BookRentalLogic.Validations
{
    public class ReadyToBeRemoved : IReadyToBeRemoved
    {
        public bool Validate(Customer entity)
        {
            return entity.CurrentlyRentedBooks.Count <= 0;
        }
    }
}
