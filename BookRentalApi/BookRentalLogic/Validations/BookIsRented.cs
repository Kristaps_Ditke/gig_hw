﻿using BookRentalAPI.Models;

namespace BookRentalLogic.Validations
{
    public class BookIsRented : IBookIsRented
    {
        public bool Validate(Book entity)
        {
            return ! entity.IsAvailable;
        }
    }
}
