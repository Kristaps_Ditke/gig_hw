﻿using BookRentalAPI;

namespace BookRentalLogic.Validations
{
    public interface IAvailableToRent : IValidator<Customer>
    {
    }
}
