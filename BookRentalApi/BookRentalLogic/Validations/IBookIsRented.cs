﻿using BookRentalAPI.Models;

namespace BookRentalLogic.Validations
{
    public interface IBookIsRented : IValidator<Book>
    {
    }
}
