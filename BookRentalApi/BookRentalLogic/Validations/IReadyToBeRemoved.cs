﻿using BookRentalAPI;

namespace BookRentalLogic.Validations
{
    public interface IReadyToBeRemoved : IValidator<Customer>
    {
    }
}
