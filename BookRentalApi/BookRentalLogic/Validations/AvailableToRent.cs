﻿using BookRentalAPI;

namespace BookRentalLogic.Validations
{
    public class AvailableToRent : IAvailableToRent
    {
        public bool Validate(Customer entity)
        {
            return entity.CurrentlyRentedBooks.Count < 2;
        }
    }
}
