﻿using System;

namespace BookRentalLogic.Exceptions
{
    public class RentLimitException : Exception
    {
        public RentLimitException(string message) : base(message)
        {
            
        }
    }
}
