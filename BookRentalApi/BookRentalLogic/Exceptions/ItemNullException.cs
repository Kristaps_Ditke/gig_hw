﻿using System;

namespace BookRentalLogic.Exceptions
{
    public class ItemNullException : Exception
    {
        public ItemNullException(string message) : base(message)
        {
        }

        public ItemNullException() : base("Id nonexistent in the database.")
        {
        }
    }
}
