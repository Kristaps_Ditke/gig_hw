﻿using System;

namespace BookRentalLogic.Exceptions
{
    public class BookRentedException : Exception
    {
        public BookRentedException(string message) : base(message)
        {
        }
    }
}
