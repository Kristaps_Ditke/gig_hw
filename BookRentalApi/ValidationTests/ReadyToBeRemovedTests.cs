﻿using BookRentalAPI;
using BookRentalAPI.Models;
using BookRentalLogic.Validations;
using NUnit.Framework;

namespace ValidationTests
{
    public class ReadyToBeRemovedTests
    {
        private ReadyToBeRemoved _validation;
        public ReadyToBeRemovedTests()
        {
            _validation = new ReadyToBeRemoved();
        }

        [Test]
        public void Validate_ReturnsFalseWhenCustomerHaveBooksRented()
        {
            //Arrange
            var customer = new Customer();
            customer.CurrentlyRentedBooks.Add(new Book() { Name = "", Author = "" });

            //Assert
            Assert.AreEqual(false, _validation.Validate(customer));
        }

        [Test]
        public void Validate_ReturnsTrueWhenCustomerHaveNoBooksRented()
        {
            //Arrange
            var customer = new Customer();

            //Assert
            Assert.AreEqual(true, _validation.Validate(customer));
        }
    }
}
