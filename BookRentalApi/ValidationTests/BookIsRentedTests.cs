﻿using BookRentalAPI;
using BookRentalAPI.Models;
using BookRentalLogic.Validations;
using NUnit.Framework;

namespace ValidationTests
{
    public class BookIsRentedTests
    {
        private BookIsRented _validate;
        public BookIsRentedTests()
        {
            _validate = new BookIsRented();
        }
        [Test]
        public void Validate_ReturnsFalseWhenIsAvailableToRent()
        {
            //Arrange
            var book = new Book();
            //Act
            var result = _validate.Validate(book);
            //Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void Validate_ReturnsTrueWhenIsRented()
        {
            //Arrange
            var book = new Book() { RentedTo = new Customer() };
            //Act
            var result = _validate.Validate(book);
            //Assert
            Assert.That(result, Is.True);
        }
    }
}
