﻿using BookRentalAPI;
using BookRentalAPI.Models;
using BookRentalLogic.Validations;
using NUnit.Framework;

namespace ValidationTests
{
    public class AvailableToRentTests
    {
        private AvailableToRent _validation;
        public AvailableToRentTests()
        {
            _validation = new AvailableToRent();
        }

        [Test]
        public void Validate_FalseWhenListCountEquals_4()
        {
            //Arrange
            var customer = new Customer();
            for (int i = 0; i < 4; i++)
            {
                customer.CurrentlyRentedBooks.Add(new Book() { Name = "", Author = ""});
            }
            
            //Assert
            Assert.AreEqual(false, _validation.Validate(customer));
        }


        [Test]
        public void Validate_TrueWhenListCountEquals_1()
        {
            //Arrange
            var customer = new Customer();
            customer.CurrentlyRentedBooks.Add(new Book() { Name = "", Author = "" });
            
            //Assert
            Assert.AreEqual(true, _validation.Validate(customer));
        }

        [Test]
        public void Validate_TrueWhenListIsEmpty()
        {
            //Arrange
            var customer = new Customer();
            
            //Assert
            Assert.AreEqual(true, _validation.Validate(customer));
        }
    }
}
