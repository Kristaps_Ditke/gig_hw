﻿using System;
using AutoMapper;
using BookRentalAPI.Dto;
using BookRentalBase;
using BookRentalBase.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookRentalAPI.Controllers
{
    [ApiController]
    [Route("customer")]
    public class CustomerController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICustomerService _service;

        public CustomerController(IMapper mapper, ICustomerService service)
        {
            _mapper = mapper;
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddCustomerRequest givenCustomer)
        {
            try
            {
                var response = await _service.CreateAsync<Customer>(_mapper.Map<Customer>(givenCustomer));
                return Created("", _mapper.Map<AddCustomerResponse>(response));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomer(int id)
        {
            try
            {
                return Ok(_mapper.Map<GetCustomerResponse>(await _service.GetCustomerByIdAsync(id)));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCustomers()
        {
            try
            {
                return Ok(_mapper.Map<List<PutCustomerResponse>>(await _service.GetAsync<Customer>()));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> PutCustomer([FromBody] PutCustomerRequest toUpdate)
        {
            try
            {
                return Ok(_mapper.Map<PutCustomerResponse>(await _service.UpdateAsync<Customer>(_mapper.Map<Customer>(toUpdate))));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            try
            {
                return Ok(_mapper.Map<DeleteCustomerResponse>(await _service.DeleteCustomerByIdAsync(id)));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
