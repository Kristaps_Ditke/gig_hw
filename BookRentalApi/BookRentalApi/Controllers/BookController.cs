﻿using AutoMapper;
using BookRentalAPI.Dto;
using BookRentalAPI.Models;
using BookRentalBase;
using BookRentalBase.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookRentalAPI.Controllers
{

    [ApiController]
    [Route("book")]
    public class BookController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IBookService _service;
        
        public BookController(IMapper mapper, IBookService service)
        {
            _mapper = mapper;
            _service = service;
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddBookRequest givenBook)
        {
            try
            {
                var result = _mapper.Map<AddBookResponse>(await _service.CreateAsync(_mapper.Map<Book>(givenBook)));
                return Created("",result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBook(int id)
        {
            try
            {
                return Ok(_mapper.Map<GetBookResponse>(await _service.GetBookByIdAsync(id)));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBooks()
        {
            try
            {
                return Ok(_mapper.Map<List<AddBookResponse>>(await _service.GetAsync<Book>()));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpPut]
        public async Task<IActionResult> PutBook([FromBody] PutBookRequest toUpdate)
        {
            try
            {
                return Ok(_mapper.Map<PutBookResponse>(await _service.UpdateAsync<Book>(_mapper.Map<Book>(toUpdate))));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteBook(int id)
        {
            try
            {
                return Ok(_mapper.Map<GetBookResponse>(await _service.DeleteAsync<Book>(id)));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
