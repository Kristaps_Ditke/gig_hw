﻿using AutoMapper;
using BookRentalAPI.Dto;
using BookRentalBase;
using BookRentalBase.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BookRentalAPI.Controllers
{
    [ApiController]
    [Route("rent")]
    public class RentController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IBookService _service;

        public RentController(IMapper mapper, IBookService service)
        {
            _mapper = mapper;
            _service = service;
        }

        [HttpPut]
        public async Task<IActionResult> RentBook([FromBody] RentBookRequest toRent)
        {
            try
            {
                return Ok(_mapper.Map<GetBookResponse>(await _service.RentBookAsync(toRent.CustomerId, toRent.BookId)));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("return/{id}")]
        public async Task<IActionResult> ReturnBook(int id)
        {
            try
            {
                return Ok(_mapper.Map<GetBookResponse>(await _service.ReturnBookAsync(id)));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
