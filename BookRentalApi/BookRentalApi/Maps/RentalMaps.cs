﻿using AutoMapper;
using BookRentalAPI.Dto;
using BookRentalAPI.Models;
using BookRentalBase.Dto;

namespace BookRentalAPI.Maps
{
    public class RentalMaps : Profile
    {
        public RentalMaps()
        {
            CreateMap<AddBookRequest, Book>();
            CreateMap<AddCustomerRequest, Customer>();
            CreateMap<Book, AddBookResponse>();
            CreateMap<Customer, AddCustomerResponse>();
            CreateMap<Book, GetBookResponse>();
            CreateMap<Customer, GetCustomerResponse>();
            CreateMap<Book, PutBookResponse>();
            CreateMap<PutBookRequest, Book>();
            CreateMap<Customer, PutCustomerResponse>();
            CreateMap<PutCustomerRequest, Customer>();
            CreateMap<Book, DeleteBookResponse>();
            CreateMap<Customer, DeleteCustomerResponse>();
        }
    }
}
