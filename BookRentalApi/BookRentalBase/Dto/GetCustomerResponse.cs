﻿using System.Collections.Generic;
using BookRentalAPI.Models;

namespace BookRentalAPI.Dto
{
    public class GetCustomerResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public List<Book> CurrentlyRentedBooks { get; set; }
    }
}
