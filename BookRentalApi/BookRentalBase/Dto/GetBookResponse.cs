﻿namespace BookRentalAPI.Dto
{
    public class GetBookResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string DateAcquired { get; set; }
        public bool IsAvailable => this.RentedTo == null;
        public Customer RentedTo { get; set; }
    }
}
