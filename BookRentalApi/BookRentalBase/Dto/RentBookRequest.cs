﻿using BookRentalAPI;

namespace BookRentalBase.Dto
{
    public class RentBookRequest
    {
        public int BookId { get; set; }
        public int CustomerId { get; set; }
    }
}
