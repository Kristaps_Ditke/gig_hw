﻿namespace BookRentalAPI.Dto
{
    public class AddBookRequest
    {
        public string Name { get; set; }
        public string Author { get; set; }
    }
}
