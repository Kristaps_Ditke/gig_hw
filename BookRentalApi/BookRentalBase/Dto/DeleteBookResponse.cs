﻿namespace BookRentalBase.Dto
{
    public class DeleteBookResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string DateAcquired { get; set; }
    }
}
