﻿namespace BookRentalBase.Dto
{
    public class DeleteCustomerResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
