﻿using BookRentalAPI;

namespace BookRentalBase.Dto
{
    public class PutBookRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
    }
}
