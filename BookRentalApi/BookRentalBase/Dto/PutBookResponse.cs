﻿using BookRentalAPI;

namespace BookRentalBase.Dto
{
    public class PutBookResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string DateAcquired { get; set; }
    }
}
