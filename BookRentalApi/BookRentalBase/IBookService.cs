﻿using System.Threading.Tasks;
using BookRentalAPI.Models;

namespace BookRentalBase
{
    public interface IBookService : IDbService
    {
        Task<Book> GetBookByIdAsync(int id);
        Task<Book> RentBookAsync(int customerId, int bookId);
        Task<Book> ReturnBookAsync(int bookId);
        Task<Book> CreateAsync(Book book);
    }
}
