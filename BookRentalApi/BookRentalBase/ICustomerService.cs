﻿using System.Threading.Tasks;
using BookRentalAPI;

namespace BookRentalBase
{
    public interface ICustomerService : IDbService
    {
        Task<Customer> GetCustomerByIdAsync(int id);
        Task<Customer> DeleteCustomerByIdAsync(int id);
    }
}
