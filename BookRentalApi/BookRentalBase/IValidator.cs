﻿using BookRentalBase.Models;

namespace BookRentalLogic.Validations
{
    public interface IValidator<in T> where T : Entity
    {
        bool Validate(T entity);
    }
}
