﻿using BookRentalBase.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookRentalBase
{
    public interface IDbService
        {
            IQueryable<T> Query<T>() where T : Entity;
            Task<IEnumerable<T>> GetAsync<T>() where T : Entity;
            Task<T> GetByIdAsync<T>(int id) where T : Entity;
            Task<T> CreateAsync<T>(T entity) where T : Entity;
            Task<T> UpdateAsync<T>(T entity) where T : Entity;
            Task<T> DeleteAsync<T>(int id) where T : Entity;
        }
}
