﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BookRentalAPI.Models;
using BookRentalBase.Models;

namespace BookRentalAPI
{
    public class Customer : Entity
    { 
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Email { get; set; }
        [Required]
        public string Address { get; set; }
        public List<Book> CurrentlyRentedBooks { get; set; } = new List<Book>();
    }
}
