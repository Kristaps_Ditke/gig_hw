﻿using BookRentalBase.Models;
using System.ComponentModel.DataAnnotations;

namespace BookRentalAPI.Models
{
    public class Book : Entity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Author { get; set; }
        public string DateAcquired { get; set; }
        public bool IsAvailable => this.RentedTo == null;
        public Customer RentedTo { get; set; }
    }
}
