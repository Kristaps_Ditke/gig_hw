## Assumptions ##

* Assuming that frontend takes care about input data types for adding new items. Any mistakes in entered data is available for correction through update.
* Get Book or Customer works using Id assuming that the names can repeate. In case of books there could be two coppies of the same book.
* Book can be deleted even if not returned in case of getting lost or may be bought out.
* Assumed that only relevant information should be available in update confirmation and full list of items request. Full information available only when requested item by Id. DTO's taking care of that.
* Used Entity model that would allow another item to be added later on. Which also would be able to use DbService logic to build upon.
* Assumed that API is available only for users with no seperation in restriction levels to information access. So there is no authorisation involved in the solution.

* With focuss on code part I havent managed to create detailed description of runing the code. Will be happy to discuss that part if needed.