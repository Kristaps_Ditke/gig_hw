# README #

* Web API for book rental operations
### Description of the Coding Challenge ###
Create a simple Book Rental Management System where the librarian can manage customers and book rentals.  
Requirements Create a web API that exposes the following controllers: 
#### Customer Controller ####
* Add a customer
* Get a customer including any rented books
* Get all customers
* Update a customer
* Remove a customer
#### Book Controller ####
* Add a book
* Get a book including whether it is rented out to a customer or not. If rented, include customer details
* Get all books
* Update a book
* Remove a book
#### Rent Controller ####

* Rent a book
* Return a book
#### Validations ####
* A book can only be rented out by one customer at any point in time 
* A customer is only allowed to rent up to two books simultaneously 
* A customer cannot be removed if he is currently renting any books 
#### Technical Requirements ####
* Web API should be developed using either .NetCore 3.1 or .NET 5.0 
* Database can be an in memory (using EntityFramework) or using sqlite 
* Solution should follow separation of concerns design principles using an n-tier architecture or an Onion architecture 
* Use Swagger to document your API (https://swagger.io/) 
* No Front End is needed. Swagger is enough. 
  
The solution should have unit tests to cover the validation scenarios. There is no need to cover additional scenarios. 
#### Showcase the use of #### 
* Base class 
* Inheritance 
* Interfaces 
* Dependency Injection 

Development Guidelines The top level of your code-base should contain a ﬁle called README, which should explain how to run your code and tests, and a ﬁle called ASSUMPTIONS, which details any assumptions you made during implementation. 

Submission Guidelines At GIG we are firm believers in Version Control Systems.  We would also like to see how the solution evolved.  As such, we would like you to use BitBucket (Git) https://bitbucket.org/ to manage your version history.  After you create a BitBucket repository, please ensure that you make regular commits of your code with descriptive messages to ensure that we can understand how your solution evolved as you tackled the requirements.  We will give you our email addresses so that you can invite us to the repo. 
 